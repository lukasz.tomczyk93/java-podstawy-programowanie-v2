package task10;

import shared.SDANumber;

import java.util.Scanner;

class MainTask10 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a positive number: ");
        int number = scanner.nextInt();

        var sdaNumber = new SDANumber(number);
        System.out.println(sdaNumber.calculateSumOfDigits());
    }
}
