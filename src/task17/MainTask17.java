package task17;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

class MainTask17 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter the next class date: ");
        var dateAsString = scanner.next();
        long daysLeftToNextClass = calculatePeriodInDays(dateAsString);
        System.out.println(String.format("Number od hours left to the next class: %s", daysLeftToNextClass));
    }

    private static long calculatePeriodInDays(String dateAsString) {
        var dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate nextClassDate = LocalDate.parse(dateAsString, dateFormat);
        LocalDate today = LocalDate.now();
        long daysLeftToNextClass = Period.between(today, nextClassDate).get(ChronoUnit.DAYS);
        //int daysLeftToNextClass = Period.between(today, nextClassDate).getDays();
        return daysLeftToNextClass;
    }
}
