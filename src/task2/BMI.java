package task2;

class BMI {

    private final double bmiValue;

    BMI(float weight, float height) {
        float heightInCm = height / 100;
        bmiValue = weight / (Math.pow(heightInCm, 2));
    }

    boolean isOptimal() {
        return bmiValue >= 18.5 && bmiValue <= 24.9;
    }
}
