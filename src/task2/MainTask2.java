package task2;

import java.util.Scanner;

class MainTask2 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter weight [kg]: ");
        var weight = scanner.nextFloat();

        System.out.print("Enter height [cm]: ");
        var height = scanner.nextFloat();

        var bmi = new BMI(weight, height);

        if (bmi.isOptimal()) {
            System.out.println("BMI optimal");
        } else {
            System.out.println("BMI not optimal");
        }
    }
}
