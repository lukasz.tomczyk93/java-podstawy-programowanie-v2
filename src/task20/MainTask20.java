package task20;

import java.util.Random;
import java.util.Scanner;

class MainTask20 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        var random = new Random();
        int randomNumber = random.nextInt(101);
        //System.out.println(String.format("The random number: %s", randomNumber));
        int userNumber;

        do {
            System.out.print("Enter the number: ");
            userNumber = scanner.nextInt();
            if (userNumber < randomNumber) {
                System.out.println("Too low");
            }
            if (userNumber > randomNumber) {
                System.out.println("Too high");
            }
        } while (userNumber != randomNumber);

        System.out.println("Bingo!");
    }
}
